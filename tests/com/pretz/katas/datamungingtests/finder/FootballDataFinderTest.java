package com.pretz.katas.datamungingtests.finder;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.pretz.katas.datamunging.finder.BaseDataFinder;
import com.pretz.katas.datamunging.finder.calc.AbsDiff;
import com.pretz.katas.datamunging.model.FootballData;
import com.pretz.katas.datamunging.finder.DataNotFoundException;

public class FootballDataFinderTest {
    private List<FootballData> footballData;
    private BaseDataFinder dataFinder;

    @Before
    public void init() {
        dataFinder = new BaseDataFinder(new AbsDiff());
        footballData = new ArrayList<>();
        footballData.add(new FootballData("Lech Poznań", new int[]{54, 34}));
        footballData.add(new FootballData("Legia Warszawa", new int[]{48, 27}));
        footballData.add(new FootballData("Pogoń Szczecin", new int[]{44, 35}));
        footballData.add(new FootballData("Górnik Zabrze", new int[]{51, 29}));
        footballData.add(new FootballData("Błękit Cyców", new int[]{22, 41}));
        footballData.add(new FootballData("FC Płochocin", new int[]{17, 46}));
    }

    @Test
    public void findSmallestGoalsDiffTest() {
        String outputString = dataFinder.find(footballData);
        Assert.assertEquals("Pogoń Szczecin", outputString);
    }

    @Test(expected = DataNotFoundException.class)
    public void emptyFootballDataTest() {
        String outputString = dataFinder.find(new ArrayList<>());
    }
}