package com.pretz.katas.datamungingtests.finder;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.pretz.katas.datamunging.finder.BaseDataFinder;
import com.pretz.katas.datamunging.finder.calc.Diff;
import com.pretz.katas.datamunging.model.WeatherData;
import com.pretz.katas.datamunging.finder.DataNotFoundException;

public class WeatherDataFinderTest {
    private List<WeatherData> weatherData;
    private BaseDataFinder dataFinder;

    @Before
    public void init() {
        dataFinder = new BaseDataFinder(new Diff());
        weatherData = new ArrayList<>();
        weatherData.add(new WeatherData("First", new int[]{20, 12}));
        weatherData.add(new WeatherData("Second", new int[]{14, 12}));
        weatherData.add(new WeatherData("Third", new int[]{21, 2}));
        weatherData.add(new WeatherData("Fourth", new int[]{14, 13}));
        weatherData.add(new WeatherData("Fifth", new int[]{5, 2}));
        weatherData.add(new WeatherData("Sixth", new int[]{10, 5}));
    }

    @Test
    public void findSmallestTempDiffTest() {
        String outputString = dataFinder.find(weatherData);
        Assert.assertEquals("Fourth", outputString);
    }

    @Test(expected = DataNotFoundException.class)
    public void emptyWeatherDataTest() {
        String outputString = dataFinder.find(new ArrayList<>());
    }
}
