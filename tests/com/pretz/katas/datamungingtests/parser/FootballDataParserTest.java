package com.pretz.katas.datamungingtests.parser;

import org.junit.Assert;
import org.junit.Test;

import com.pretz.katas.datamunging.model.FootballData;
import com.pretz.katas.datamunging.parser.FootballDataParser;


public class FootballDataParserTest {
    private FootballDataParser dataParser;

    @Test
    public void properInputRowTest() {
        String inputRow = "    6. Chelsea         38    17  13   8    66  -  38    64";
        dataParser = new FootballDataParser();
        FootballData parsedValues = dataParser.parse(inputRow);
        FootballData expectedValues = new FootballData("Chelsea", new int[]{66, 38});
        Assert.assertEquals(expectedValues, parsedValues);
    }

    @Test
    public void discardedInputRowTest() {
        String inputRowHeaders = "       Team            P     W    L   D    F      A     Pts";
        String inputRowDashes = "-------------------------------------------------------";
        dataParser = new FootballDataParser();
        FootballData parsedHeadersValues = dataParser.parse(inputRowHeaders);
        FootballData parsedDashesValues = dataParser.parse(inputRowDashes);
        Assert.assertNull(parsedHeadersValues);
        Assert.assertNull(parsedDashesValues);
    }

    @Test
    public void malformedInputRowTest() {
        String malformedInputRow = "sgerhr3fwhergwef";
        dataParser = new FootballDataParser();
        FootballData parsedValues = dataParser.parse(malformedInputRow);
        Assert.assertNull(parsedValues);
    }
}
