package com.pretz.katas.datamungingtests.parser;

import org.junit.Assert;
import org.junit.Test;

import com.pretz.katas.datamunging.model.WeatherData;
import com.pretz.katas.datamunging.parser.WeatherDataParser;

public class WeatherDataParserTest {
    private WeatherDataParser dataParser;

    @Test
    public void properInputRowTest() {
        String inputRow = "   4  77    59    68          51.1       0.00         110  9.1 130  12  8.6  62 40 1021.1";
        dataParser = new WeatherDataParser();
        WeatherData parsedValues = dataParser.parse(inputRow);
        WeatherData expectedValues = new WeatherData("4", new int[]{77, 59});
        Assert.assertEquals(expectedValues, parsedValues);
    }

    @Test
    public void properInputRowWithRegexTest() {
        String inputRow = "  26  97*   64    81          70.4       0.00 H       050  5.1 200  12  4.0 107 45 1014.9";
        dataParser = new WeatherDataParser();
        WeatherData parsedValues = dataParser.parse(inputRow);
        WeatherData expectedValues = new WeatherData("26", new int[]{97, 64});
        Assert.assertEquals(expectedValues, parsedValues);
    }

    @Test
    public void properInputRowWithAdditionalColumnsTest() {
        String inputRow = "  14  61    59    60       5  55.9       0.00 RF      060  6.7 080   9 10.0  93 87 1008.6";
        dataParser = new WeatherDataParser();
        WeatherData parsedValues = dataParser.parse(inputRow);
        WeatherData expectedValues = new WeatherData("14", new int[]{61, 59});
        Assert.assertEquals(expectedValues, parsedValues);
    }

    @Test
    public void discardedInputRowTest() {
        String inputRowHeaders = "  Dy MxT   MnT   AvT   HDDay  AvDP 1HrP TPcpn WxType PDir AvSp Dir MxS SkyC MxR MnR AvSLP";
        String inputRowMean = "  mo  82.9  60.5  71.7    16  58.8       0.00              6.9          5.3";
        String inputRowEmpty = "";
        dataParser = new WeatherDataParser();
        WeatherData parsedHeadersValues = dataParser.parse(inputRowHeaders);
        WeatherData parsedDashesValues = dataParser.parse(inputRowMean);
        WeatherData parsedEmptyValues = dataParser.parse(inputRowEmpty);
        Assert.assertNull(parsedHeadersValues);
        Assert.assertNull(parsedDashesValues);
        Assert.assertNull(parsedEmptyValues);
    }

    @Test
    public void malformedInputRowTest() {
        String malformedInputRow = "sht4h5644224h";
        dataParser = new WeatherDataParser();
        WeatherData parsedValues = dataParser.parse(malformedInputRow);
        Assert.assertNull(parsedValues);
    }
}
