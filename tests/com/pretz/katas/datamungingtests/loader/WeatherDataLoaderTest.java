package com.pretz.katas.datamungingtests.loader;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.pretz.katas.datamunging.loader.BaseDataLoader;
import com.pretz.katas.datamunging.model.WeatherData;
import com.pretz.katas.datamunging.model.InputData;
import com.pretz.katas.datamunging.parser.DataParser;
import com.pretz.katas.datamunging.parser.WeatherDataParser;

public class WeatherDataLoaderTest {
    private String inputPath;
    private BaseDataLoader dataLoader;
    private List<WeatherData> expectedData;
    private WeatherDataParser weatherDataParser;

    @Before
    public void init() {
        inputPath = "res/test/weather_test.dat";

        expectedData = new ArrayList<>();
        expectedData.add(new WeatherData("1", new int[]{88, 59}));
        expectedData.add(new WeatherData("2", new int[]{79, 63}));
        expectedData.add(new WeatherData("3", new int[]{77, 55}));

        weatherDataParser = new WeatherDataParser();
    }

    @Test
    public void parseInputToWeatherDataListTest() throws FileNotFoundException {
        dataLoader = new BaseDataLoader(inputPath, weatherDataParser);
        List<InputData> parsedData = dataLoader.load();
        Assert.assertEquals(expectedData, parsedData);
    }

    @Test(expected = FileNotFoundException.class)
    public void emptyInputDataTest() throws FileNotFoundException {
        dataLoader = new BaseDataLoader("", weatherDataParser);
        List<InputData> parsedData = dataLoader.load();
    }

    private class MockWeatherDataParser implements DataParser {
        @Override
        public InputData parse(String input) {
            return null;
        }
    }
}
