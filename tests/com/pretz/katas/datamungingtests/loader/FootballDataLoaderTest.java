package com.pretz.katas.datamungingtests.loader;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.pretz.katas.datamunging.parser.FootballDataParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.pretz.katas.datamunging.loader.BaseDataLoader;
import com.pretz.katas.datamunging.model.InputData;
import com.pretz.katas.datamunging.model.FootballData;
import com.pretz.katas.datamunging.parser.DataParser;

public class FootballDataLoaderTest {
    private String inputPath;
    private BaseDataLoader dataLoader;
    private List<FootballData> expectedData;
    private FootballDataParser footballDataParser;

    @Before
    public void init() {
        inputPath = "res/test/football_test.dat";

        expectedData = new ArrayList<>();
        expectedData.add(new FootballData("Arsenal", new int[]{79, 36}));
        expectedData.add(new FootballData("Liverpool", new int[]{67, 30}));
        expectedData.add(new FootballData("Manchester_U", new int[]{87, 45}));

        footballDataParser = new FootballDataParser();
    }

    @Test
    public void parseInputToFootballDataListTest() throws FileNotFoundException {
        dataLoader = new BaseDataLoader(inputPath, footballDataParser);
        List<InputData> parsedData = dataLoader.load();
        Assert.assertEquals(expectedData, parsedData);
    }

    @Test(expected = FileNotFoundException.class)
    public void emptyInputDataTest() throws FileNotFoundException {
        dataLoader = new BaseDataLoader("", footballDataParser);
        List<InputData> parsedData = dataLoader.load();
    }

    private class MockFootballDataParser implements DataParser<FootballData> {
        @Override
        public FootballData parse(String input) {
            return null;
        }
    }
}
