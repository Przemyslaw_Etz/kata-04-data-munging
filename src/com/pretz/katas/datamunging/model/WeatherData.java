package com.pretz.katas.datamunging.model;

import java.util.Arrays;

public class WeatherData implements InputData {
    private String key;
    private int[] values;

    public WeatherData(String key, int[] values) {
        this.key = key;
        this.values = values;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public int[] getValues() {
        return values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WeatherData that = (WeatherData) o;

        if (!key.equals(that.key)) return false;
        return Arrays.equals(values, that.values);
    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        result = 31 * result + Arrays.hashCode(values);
        return result;
    }
}
