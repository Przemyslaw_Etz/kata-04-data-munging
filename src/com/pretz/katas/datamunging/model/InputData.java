package com.pretz.katas.datamunging.model;

public interface InputData {
    String getKey();
    int[] getValues();
}
