package com.pretz.katas.datamunging.loader;

import java.io.FileNotFoundException;
import java.util.List;

import com.pretz.katas.datamunging.model.InputData;

public interface DataLoader<T extends InputData> {

    /**
     * Loads data from file.
     *
     * @return InputData parsed to DataFinder input format.
     */
    public List<T> load() throws FileNotFoundException;
}
