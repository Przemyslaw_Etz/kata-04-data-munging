package com.pretz.katas.datamunging.loader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.pretz.katas.datamunging.model.InputData;
import com.pretz.katas.datamunging.parser.DataParser;

public class BaseDataLoader implements DataLoader<InputData> {
    private String path;
    private DataParser dataParser;

    public BaseDataLoader(String path, DataParser dataParser) {
        this.path = path;
        this.dataParser = dataParser;
    }

    @Override
    public List<InputData> load() throws FileNotFoundException {
        Scanner sc = new Scanner(new File(path));
        List<String> lines = new ArrayList<>();
        while (sc.hasNextLine()) {
            lines.add(sc.nextLine());
        }
        List<InputData> data = lines.stream().map((String input) -> dataParser.parse(input)).filter(Objects::nonNull).collect(Collectors.toList());
        sc.close();
        return data;
    }
}
