package com.pretz.katas.datamunging.loader;

import java.io.IOException;

import com.pretz.katas.datamunging.model.InputData;
import com.pretz.katas.datamunging.parser.FootballDataParser;
import com.pretz.katas.datamunging.parser.WeatherDataParser;

public class DataLoaderFactory {

    public DataLoader<InputData> getDataLoader(String path) throws IOException {
        if (path.matches("(.*weather.*)")) {
            return new BaseDataLoader(path, new WeatherDataParser());
        }
        else if (path.matches(".*football.*")) {
            return new BaseDataLoader(path, new FootballDataParser());
        }
        else throw new IOException("wrong input data");
    }
}
