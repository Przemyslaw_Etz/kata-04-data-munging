package com.pretz.katas.datamunging.finder;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.pretz.katas.datamunging.finder.calc.CalcMethod;
import com.pretz.katas.datamunging.model.InputData;

public class BaseDataFinder implements DataFinder<InputData> {
    private CalcMethod calcMethod;

    public BaseDataFinder(CalcMethod calcMethod) {
        this.calcMethod = calcMethod;
    }

    @Override
    public String find(List<? extends InputData> dataList) {
        Optional<? extends InputData> foundRow = dataList.stream().min(Comparator.comparingInt(d -> calcMethod.calc(d.getValues())));
        if (foundRow.isPresent()) {
            return foundRow.get().getKey();
        } else {
            throw new DataNotFoundException();
        }
    }
}
