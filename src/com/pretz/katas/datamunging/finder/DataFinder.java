package com.pretz.katas.datamunging.finder;

import java.util.List;

import com.pretz.katas.datamunging.model.InputData;

public interface DataFinder<T extends InputData> {

    /**
     * Finds name of row fulfilling some arithmetic condition (f.e. smallest difference between two columns).
     *
     * @param dataList InputData to be processed, abstracting list of rows from file input.
     * @return name of row fulfilling the condition.
     */
    public String find(List<? extends T> dataList);
}
