package com.pretz.katas.datamunging.finder.calc;

public class AbsDiff implements CalcMethod {
    @Override
    public int calc(int[] values) {
        return Math.abs(values[0] - values[1]);
    }
}
