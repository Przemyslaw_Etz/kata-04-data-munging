package com.pretz.katas.datamunging.finder.calc;

public class Diff implements CalcMethod {
    @Override
    public int calc(int[] values) {
        return values[0] - values[1];
    }
}
