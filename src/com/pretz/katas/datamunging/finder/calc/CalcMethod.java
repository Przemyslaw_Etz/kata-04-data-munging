package com.pretz.katas.datamunging.finder.calc;

public interface CalcMethod {
    int calc(int[] values);
}

