package com.pretz.katas.datamunging.finder;

import java.io.IOException;

import com.pretz.katas.datamunging.finder.calc.AbsDiff;
import com.pretz.katas.datamunging.finder.calc.Diff;
import com.pretz.katas.datamunging.model.InputData;

public class DataFinderFactory {

    public DataFinder<InputData> getDataFinder(String path) throws IOException {
        if (path.matches(".*weather.*")) {
            return new BaseDataFinder(new Diff());
        }
        else if (path.matches(".*football.*")) {
            return new BaseDataFinder(new AbsDiff());
        }
        else throw new IOException("wrong input data");
    }
}
