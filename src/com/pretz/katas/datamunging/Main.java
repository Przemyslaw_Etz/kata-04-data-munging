package com.pretz.katas.datamunging;

import java.io.IOException;

import com.pretz.katas.datamunging.finder.DataFinderFactory;
import com.pretz.katas.datamunging.loader.DataLoaderFactory;

public class Main {

    public static void main(String[] args) throws IOException {
        String weatherPath = "res/weather.dat";
        String weatherAnswer = new DataFinderFactory().getDataFinder(weatherPath).find(
                new DataLoaderFactory().getDataLoader(weatherPath).load());
        System.out.println(weatherAnswer);

        String footballPath = "res/football.dat";
        String footballAnswer = new DataFinderFactory().getDataFinder(footballPath).find(
                new DataLoaderFactory().getDataLoader(footballPath).load());
        System.out.println(footballAnswer);
    }
}
