package com.pretz.katas.datamunging.parser;

import java.util.Scanner;

import com.pretz.katas.datamunging.model.WeatherData;

public class WeatherDataParser implements DataParser<WeatherData> {
    private Scanner sc;

    public WeatherData parse(String inputRow) {
        this.sc = new Scanner(inputRow);
        if (validateRow()) {
            String[] inputs = inputRow.split("\\** +");
            return new WeatherData(inputs[1], new int[]{Integer.parseInt(inputs[2]), Integer.parseInt(inputs[3])});
        } else {
            return null;
        }
    }

    private boolean validateRow() {
        return sc.hasNextInt();
    }
}
