package com.pretz.katas.datamunging.parser;

import java.util.Scanner;

import com.pretz.katas.datamunging.model.FootballData;

public class FootballDataParser implements DataParser<FootballData> {
    private Scanner sc;

    public FootballData parse(String inputRow) {
        this.sc = new Scanner(inputRow);
        if (validateRow()) {
            String[] inputs = inputRow.split(" +");
            return new FootballData(inputs[2], new int[]{Integer.parseInt(inputs[7]), Integer.parseInt(inputs[9])});
        } else {
            return null;
        }
    }

    private boolean validateRow() {
        return sc.hasNext("[1-9]\\.");
    }
}
