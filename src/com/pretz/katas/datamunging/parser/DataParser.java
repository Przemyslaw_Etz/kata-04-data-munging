package com.pretz.katas.datamunging.parser;

import com.pretz.katas.datamunging.model.InputData;

public interface DataParser<T extends InputData> {

    /**
     * Parses input row represented in String into proper data row, represented as generic type.
     *
     * @param input Row from input file
     * @return InputData row to bebe used in further processing
     */
    public T parse(String input);
}
